package algorithm.sort.comparison;

import java.util.Arrays;
import java.util.List;

public class MaxSubarray {

	public static List<Integer> findMaxCrossingSubarray(List<Integer> input, int low, int mid, int high) {
		int leftSum = input.get(mid);
		int sum = leftSum;
		int maxLeft = mid;
		for (int i = mid - 1; low <= i; i--) {
			sum = sum + input.get(i);
			if (leftSum < sum) {
				leftSum = sum;
				maxLeft = i;
			}
		}
		int rightSum = input.get(mid + 1);
		sum = rightSum;
		int maxRight = mid + 1;
		for (int i = mid + 2; i <= high; i++) {
			sum = sum + input.get(i);
			if (rightSum < sum) {
				rightSum = sum;
				maxRight = i;
			}
		}
		return Arrays.asList(maxLeft, maxRight, leftSum + rightSum);
	}

	public static List<Integer> findMaximumSubarray(List<Integer> input, int low, int high) {
		if (low == high) {
			return Arrays.asList(low, high, input.get(low));
		} else {
			int mid = Math.floorDiv(low + high, 2);
			List<Integer> left = findMaximumSubarray(input, low, mid);
			List<Integer> right = findMaximumSubarray(input, mid + 1, high);
			List<Integer> cross = findMaxCrossingSubarray(input, low, mid, high);
			if (right.get(2) <= left.get(2) && cross.get(2) <= left.get(2)) {
				return left;
			} else if (left.get(2) <= right.get(2) && cross.get(2) <= right.get(2)) {
				return right;
			} else {
				return cross;
			}
		}
	}

	public static int maxSubArray(int[] A) {
		int newsum = A[0];
		int max = A[0];
		for (int i = 1; i < A.length; i++) {
			newsum = Math.max(newsum + A[i], A[i]);
			max = Math.max(max, newsum);
		}
		return max;
	}

	// dynamic programming
	public static int maxSubArrayDyn(int[] A) {
		int max = A[0];
		int[] sum = new int[A.length];
		sum[0] = A[0];

		for (int i = 1; i < A.length; i++) {
			sum[i] = Math.max(A[i], sum[i - 1] + A[i]);
			max = Math.max(max, sum[i]);
		}

		return max;
	}

}
