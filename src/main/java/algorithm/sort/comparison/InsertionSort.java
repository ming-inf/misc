package algorithm.sort.comparison;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;

import algorithm.sort.comparison.Cardinal.Ordinal;

public class InsertionSort {

	public enum SortOrder {
		ASCENDING(-1), UNSORTED(0), DESCENDING(1);

		private int comparable;

		SortOrder(int comparable) {
			this.comparable = comparable;
		}

		public int getComparable() {
			return comparable;
		}
	}

	static Predicate<Ordinal> isNonNegative = i -> 0 <= i.intValue();

	// not in place
	// stable sort
	public static <T extends Comparable<? super T>> List<T> insertionSort(List<T> input, SortOrder order) {
		int orderBy = order.getComparable();
		List<T> output = new ArrayList<>();
		for (T key : input) {
			Ordinal i = new Cardinal(output.size()).getLimit();
			while (isNonNegative.test(i) && key.compareTo(output.get(i.intValue())) == orderBy) {
				i.decrement();
			}
			output.add(i.intValue() + 1, key);
		}
		return output;
	}

	public static <T extends Comparable<? super T>> List<T> insertionSort(List<T> input) {
		return insertionSort(input, SortOrder.ASCENDING);
	}

	// in place
	public static <T extends Comparable<? super T>> List<T> insertionInPlaceSort(List<T> input, SortOrder order) {
		int orderBy = order.getComparable();
		Cardinal c = new Cardinal(input.size());
		for (Ordinal o = c.getInitial(); o.intValue() < c.intValue(); o.increment()) {
			int i = o.intValue();
			while (0 < i && input.get(i).compareTo(input.get(i - 1)) == orderBy) {
				Collections.swap(input, i, i - 1);
				i--;
			}
		}
		return input;
	}

	// use iterator
	// in place
	public static <T extends Comparable<? super T>> List<T> insertionIteratorSort(List<T> input, SortOrder order) {
		int orderBy = order.getComparable();
		for (ListIterator<T> j = input.listIterator(); j.hasNext(); j.next()) {
			int i = j.nextIndex();
			while (0 < i && input.get(i).compareTo(input.get(i - 1)) == orderBy) {
				Collections.swap(input, i, i - 1);
				i--;
			}
		}
		return input;
	}

	// use rotate
	// in place
	public static <T extends Comparable<? super T>> List<T> insertionRotateSort(List<T> input, SortOrder order) {
		int orderBy = order.getComparable();
		for (int j = 1; j < input.size(); j++) {
			int i = j;
			if (input.get(i).compareTo(input.get(j)) != orderBy) {
				while (0 < i && input.get(i - 1).compareTo(input.get(j)) != orderBy) {
					i--;
				}
			}
			Collections.rotate(input.subList(i, j + 1), 1);
		}
		return input;
	}

}
