package algorithm.sort.comparison;

import java.util.Collections;
import java.util.List;

public class HeapSort {

	public static int parent(int i) {
		return Math.floorDiv(i, 2);
	}
	public static int left(int i) {
		return 2 * i;
	}
	public static int right(int i) {
		return 2 * i + 1;
	}
	
	public static Integer heapMaximum(List<Integer> input) {
		return input.get(0);
	}
	
	public static Integer heapExtractMaximum(List<Integer> input) {
		int size = input.size();
		if (size < 1) {
			return null;
		}
		int max = input.get(0);
		input.set(0, input.get(size));
		size--;
		maxHeapify(input, 1, size);
		return max;
	}
	
	public static List<Integer> heapIncreaseKey(List<Integer> input, int i, int key) {
		if (key < input.get(i - 1)) {
			throw new RuntimeException("new key is smaller than current key");
		}
		input.set(i - 1, key);
		int newI = i - 1;
		while (0 < newI && input.get(parent(newI + 1) - 1) < input.get(newI)) {
			Collections.swap(input, newI, parent(newI + 1) - 1);
			newI = parent(newI) - 1;
		}
		return input;
	}
	
	public static List<Integer> minHeapify(List<Integer> input, int i) {
		int l = left(i) - 1;
		int r = right(i) - 1;
		int smallest;
		if (l < input.size() && input.get(l) < input.get(i - 1)) {
			smallest = l;
		} else {
			smallest = i - 1;
		}
		if (r < input.size() && input.get(r) < input.get(smallest)) {
			smallest = r;
		}
		if (smallest != i - 1) {
			Collections.swap(input, i - 1, smallest);
			return minHeapify(input, smallest + 1);
		}
		return input;
	}
	
	public static List<Integer> buildMinHeap(List<Integer> input) {
		for (int i = Math.floorDiv(input.size(), 2); i >= 1; i--) {
			minHeapify(input, i);
		}
		return input;
	}
	
	public static List<Integer> maxHeapify(List<Integer> input, int i, int size) {
		int l = left(i) - 1;
		int r = right(i) - 1;
		int largest;
		if (l < size && input.get(i - 1) < input.get(l)) {
			largest = l;
		} else {
			largest = i - 1;
		}
		if (r < size && input.get(largest) < input.get(r)) {
			largest = r;
		}
		if (largest != i - 1) {
			Collections.swap(input, i - 1, largest);
			return maxHeapify(input, largest + 1, size);
		}
		return input;
	}
	
	public static List<Integer> buildMaxHeap(List<Integer> input) {
		for (int i = Math.floorDiv(input.size(), 2); i >= 1; i--) {
			maxHeapify(input, i, input.size());
		}
		return input;
	}

	public static List<Integer> heapSort(List<Integer> input) {
		List<Integer> maxHeap = buildMaxHeap(input);
		int size = input.size();
		for (int i = input.size() - 1; i >= 1; i--) {
			Collections.swap(input, 0, i);
			maxHeapify(input, 1, --size);
		}
		return maxHeap;
	}

}
