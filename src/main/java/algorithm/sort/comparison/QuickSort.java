package algorithm.sort.comparison;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class QuickSort {

	// in place
	public static List<Integer> quickSort(List<Integer> input, int p, int r) {
		if (p < r) {
			int q = partition(input, p, r);
			quickSort(input, p, q - 1);
			quickSort(input, q + 1, r);
		}
		return input;
	}

	public static int partition(List<Integer> input, int p, int r) {
		int x = input.get(r - 1);
		int i = p - 2;
		for (int j = p - 1; j < r - 1; j++) {
			if (input.get(j) <= x) {
				i++;
				Collections.swap(input, i, j);
			}
		}
		Collections.swap(input, i + 1, r - 1);
		return i + 2;
	}

	// in place
	public static List<Integer> randomizedQuickSort(List<Integer> input, int p, int r) {
		if (p < r) {
			int q = randomizedPartition(input, p, r);
			randomizedQuickSort(input, p, q - 1);
			randomizedQuickSort(input, q + 1, r);
		}
		return input;
	}

	public static int randomizedPartition(List<Integer> input, int p, int r) {
		int i = new Random().nextInt(r - p + 1) + p;
		Collections.swap(input, i - 1, r - 1);
		return partition(input, p, r);
	}

}
