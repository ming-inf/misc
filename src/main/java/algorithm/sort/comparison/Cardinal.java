package algorithm.sort.comparison;

class Cardinal extends Number {
	private int i;

	public Cardinal(int i) {
		this.i = i;
	}

	public Ordinal getLimit() {
		return new Ordinal(i < 0 ? 0 : i - 1);
	}

	public Ordinal getInitial() {
		return new Ordinal(0);
	}

	@Override
	public int intValue() {
		return i;
	}

	@Override
	public long longValue() {
		return i;
	}

	@Override
	public float floatValue() {
		return i;
	}

	@Override
	public double doubleValue() {
		return i;
	}

	class Ordinal extends Number {
		private int i;

		Ordinal(int i) {
			this.i = i;
		}

		public void decrement() {
			i--;
		}

		public void increment() {
			i++;
		}

		public int predecessor() {
			return i - 1;
		}

		public int successor() {
			return i + 1;
		}

		@Override
		public int intValue() {
			return i;
		}

		@Override
		public long longValue() {
			return i;
		}

		@Override
		public float floatValue() {
			return i;
		}

		@Override
		public double doubleValue() {
			return i;
		}
	}
}