package algorithm.sort.comparison;

import java.util.ArrayList;
import java.util.List;

import algorithm.sort.comparison.InsertionSort.SortOrder;

public class MergeSort {

	// p <= q < r
	public static <T extends Comparable<? super T>> List<Integer> merge(List<Integer> input, int p, int q, int r) {
		ArrayList<Integer> left = new ArrayList<>();
		left.addAll(input.subList(p, q));
		ArrayList<Integer> right = new ArrayList<>();
		right.addAll(input.subList(q, r));
		left.add(Integer.MAX_VALUE);
		right.add(Integer.MAX_VALUE);
		int i = 0;
		int j = 0;
		for (int k = p; k < r; k++) {
			if (left.get(i).compareTo(right.get(j)) <= 0) {
				input.set(k, left.get(i));
				i++;
			} else {
				input.set(k, right.get(j));
				j++;
			}
		}
		return input;
	}

	// in place sort
	// stable sort
	public static <T extends Comparable<? super T>> List<Integer> mergeSort(List<Integer> input, int p, int r, SortOrder order) {
		if (p < r) {
			int q = Math.floorDiv(p + r, 2);
			mergeSort(input, p, q, order);
			mergeSort(input, q + 1, r, order);
			merge(input, p, q, r);
		}
		return input;
	}

}
