package algorithm.sort.integer;

import java.util.List;

public class RadixSort {

	public static List<Integer> radixSort(List<Integer> input, int d) {
		List<Integer> temp = input;
		for (int i = 1; i <= d; i++) {
			temp = CountingSort.countingSortByDigit(temp, i);
		}
		return temp;
	}

}
