package algorithm.sort.integer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CountingSort {

	// stable
	public static List<Integer> countingSort(List<Integer> input, int k) {
		List<Integer> c = new ArrayList<>(Collections.nCopies(k + 1, 0));
		for (int j = 0; j < input.size(); j++) {
			c.set(input.get(j), c.get(input.get(j)) + 1);
		}
		for (int i = 1; i <= k; i++) {
			c.set(i, c.get(i) + c.get(i - 1));
		}
		List<Integer> b = new ArrayList<>(Collections.nCopies(input.size(), 0));
		for (int j = input.size() - 1; j >= 0; j--) {
			b.set(c.get(input.get(j)) - 1, input.get(j));
			c.set(input.get(j), c.get(input.get(j)) - 1);
		}
		return b;
	}
	
	// k = 10
	public static List<Integer> countingSortByDigit(List<Integer> input, int d) {
		List<Integer> c = new ArrayList<>(Collections.nCopies(10 + 1, 0));
		for (int i = 0; i < input.size(); i++) {
			int digit = input.get(i) / (int)Math.pow(10, d - 1) % 10;
			c.set(digit, c.get(digit) + 1);
		}
		for (int i = 1; i < c.size(); i++) {
			c.set(i, c.get(i) + c.get(i - 1));
		}
		List<Integer> b = new ArrayList<>(Collections.nCopies(input.size(), 0));
		for (int i = input.size() - 1; i >= 0; i--) {
			int digit = input.get(i) / (int)Math.pow(10, d - 1) % 10;
			b.set(c.get(digit) - 1, input.get(i));
			c.set(digit, c.get(digit) - 1);
		}
		return b;
	}

}
