package algorithm.diff;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import com.google.common.io.ByteProcessor;

public class DigestByteProcessor implements ByteProcessor<byte[]> {
	private MessageDigest digester;

	public DigestByteProcessor() throws NoSuchAlgorithmException {
		digester = MessageDigest.getInstance("sha-512");
	}

	@Override
	public byte[] getResult() {
		return digester.digest();
	}

	@Override
	public boolean processBytes(byte[] input, int offset, int len) throws IOException {
		digester.update(input, offset, len);
		return true;
	}

	public byte[] digest(InputStream input) throws IOException {
		digester.reset();
		byte[] buf = createBuffer();
		int read;
		do {
			read = input.read(buf);
		} while (read != -1 && this.processBytes(buf, 0, read));
		return this.getResult();
	}

	byte[] createBuffer() {
		return new byte[8192];
	}

	public static Callable<byte[]> digestCallable(InputStream input) {
		return new Callable<byte[]>() {
			@Override
			public byte[] call() throws Exception {
				return new DigestByteProcessor().digest(input);
			}
		};
	}

	public static boolean compare(InputStream input1, InputStream input2)
			throws InterruptedException, ExecutionException {
		ExecutorService executor = new ScheduledThreadPoolExecutor(2);
		Future<byte[]> future1 = executor.submit(digestCallable(input1));
		Future<byte[]> future2 = executor.submit(digestCallable(input2));

		final byte[] digest1 = future1.get();
		final byte[] digest2 = future2.get();

		return Arrays.equals(digest1, digest2);
	}
}
