package netty.srp;

import java.net.SocketAddress;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.tls.TlsClientProtocol;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

public class TlsSrpClientHandler extends ChannelOutboundHandlerAdapter {
	SecureRandom secureRandom;
	TlsClientProtocol clientProtocol;
	MockSRPTlsClient tlsClient;
	
	public TlsSrpClientHandler() {
		Security.addProvider(new BouncyCastleProvider());
		secureRandom = new SecureRandom();
		clientProtocol = new TlsClientProtocol(secureRandom);
		tlsClient = new MockSRPTlsClient(null, MockSRPTlsServer.TEST_IDENTITY,
				MockSRPTlsServer.TEST_PASSWORD);
	}

	@Override
	public void connect(ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress,
			ChannelPromise promise) throws Exception {
		clientProtocol.connect(tlsClient);
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		byte[] bb = (byte[]) msg;
		clientProtocol.offerOutput(bb, 0, bb.length);
		byte[] out = new byte[clientProtocol.getAvailableOutputBytes()];
		clientProtocol.readOutput(out, 0, out.length);
		ctx.write(out);
	}
}
