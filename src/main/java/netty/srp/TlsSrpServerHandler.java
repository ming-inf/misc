package netty.srp;

import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.tls.TlsServerProtocol;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class TlsSrpServerHandler extends ChannelInboundHandlerAdapter {
	SecureRandom secureRandom;
	TlsServerProtocol serverProtocol;
	MockSRPTlsServer tlsServer;
	
	public TlsSrpServerHandler() {
		Security.addProvider(new BouncyCastleProvider());
		secureRandom = new SecureRandom();
		serverProtocol = new TlsServerProtocol(secureRandom);
		tlsServer = new MockSRPTlsServer();
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Here");
		serverProtocol.accept(tlsServer);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		System.out.println("Here");
		serverProtocol.accept(tlsServer);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf bb = (ByteBuf) msg;
		serverProtocol.offerInput(bb.array());
		byte[] output = new byte[serverProtocol.getAvailableInputBytes()];
		serverProtocol.readInput(output, 0, output.length);
		ctx.write(output);
	}
}
