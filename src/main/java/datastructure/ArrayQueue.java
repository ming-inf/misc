package datastructure;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;

import datastructure.api.Queue;

public class ArrayQueue<T> implements Queue<T> {
	private List<T> queue = new ArrayList<>(10);
	private int head = 0;
	private int tail = 0;

	@Override
	public void enqueue(T element) {
		queue.add(tail, element);
		tail = ++tail % queue.size();
	}

	@Override
	public Optional<T> dequeue() {
		T dequeued = queue.get(head);
		queue.set(head, null);
		head = ++head % queue.size();
		return Optional.fromNullable(dequeued);
	}

}
