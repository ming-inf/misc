package datastructure;

import com.google.common.base.Optional;

import datastructure.api.LinkedList;
import datastructure.api.LinkedListNode;

public class SentinelLinkedList<T> implements LinkedList<T> {
	private LinkedListNode<T> nil = new LinkedListNode<T>(null);

	public SentinelLinkedList() {
		nil.setNext(nil);
		nil.setPrev(nil);
	}

	@Override
	public Optional<LinkedListNode<T>> search(T element) {
		LinkedListNode<T> x = nil.getNext();
		while (x != null && x.get() != element) {
			x = x.getNext();
		}
		return Optional.fromNullable(x);
	}

	@Override
	public void insert(T element) {
		LinkedListNode<T> x = new LinkedListNode<T>(element);
		x.setNext(nil.getNext());
		nil.getNext().setPrev(x);
		nil.setNext(x);
		x.setPrev(nil);
	}

	@Override
	public Optional<T> delete(T element) {
		Optional<LinkedListNode<T>> found = search(element);
		if (found.isPresent()) {
			LinkedListNode<T> x = found.get();
			x.getPrev().setNext(x.getNext());
			x.getNext().setPrev(x.getPrev());
			return Optional.fromNullable(x.get());
		} else {
			return Optional.absent();
		}
	}

}
