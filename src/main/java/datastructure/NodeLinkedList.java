package datastructure;

import com.google.common.base.Optional;

import datastructure.api.LinkedList;
import datastructure.api.LinkedListNode;

public class NodeLinkedList<T> implements LinkedList<T> {
	private LinkedListNode<T> head = null;

	@Override
	public Optional<LinkedListNode<T>> search(T element) {
		LinkedListNode<T> x = head;
		while (x != null && x.get() != element) {
			x = x.getNext();
		}
		return Optional.fromNullable(x);
	}

	@Override
	public void insert(T element) {
		LinkedListNode<T> x = new LinkedListNode<T>(element);
		x.setNext(head);
		if (head != null) {
			head.setPrev(x);
		}
		head = x;
		x.setPrev(null);
	}

	@Override
	public Optional<T> delete(T element) {
		Optional<LinkedListNode<T>> found = search(element);
		if (found.isPresent()) {
			LinkedListNode<T> x = found.get();
			if (x.getPrev() != null) {
				x.getPrev().setNext(x.getNext());
			} else {
				head = x.getNext();
			}
			if (x.getNext() != null) {
				x.getNext().setPrev(x.getPrev());
			}
			return Optional.fromNullable(x.get());
		} else {
			return Optional.absent();
		}
	}

}
