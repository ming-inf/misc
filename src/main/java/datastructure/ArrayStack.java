package datastructure;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;

import datastructure.api.Stack;

public class ArrayStack<T> implements Stack<T> {
	private List<T> stack = new ArrayList<>();

	public ArrayStack() {
	}

	@Override
	public void push(T element) {
		stack.add(element);
	}

	@Override
	public Optional<T> pop() {
		if (stack.isEmpty()) {
			return Optional.absent();
		}
		return Optional.of(stack.remove(stack.size() - 1));
	}

	@Override
	public boolean stackEmpty() {
		return stack.isEmpty();
	}

}
