package datastructure.api;

import com.google.common.base.Optional;

public interface Queue<T> {
	public void enqueue(T element);
	public Optional<T> dequeue();
}
