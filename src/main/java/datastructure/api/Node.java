package datastructure.api;

import java.util.List;

public class Node<T> {

	protected T key;
	protected List<Node<T>> children;

	public Node(T key) {
		this.key = key;
	}

	public T get() {
		return key;
	}

	public List<Node<T>> getChildren() {
		return children;
	}

}