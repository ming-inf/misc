package datastructure.api;

import com.google.common.base.Optional;

public interface LinkedList<T> {
	public Optional<LinkedListNode<T>> search(T element);
	public void insert(T element);
	public Optional<T> delete(T element);
}
