package datastructure.api;

import com.google.common.base.Optional;

public interface Stack<T> {
	public void push(T element);
	public Optional<T> pop();
	public boolean stackEmpty();
}
