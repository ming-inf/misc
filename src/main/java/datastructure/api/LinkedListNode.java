package datastructure.api;

public class LinkedListNode<T> {
	private T key;
	private LinkedListNode<T> prev;
	private LinkedListNode<T> next;

	public LinkedListNode(T key) {
		this.key = key;
	}

	public T get() {
		return key;
	}

	public LinkedListNode<T> getPrev() {
		return prev;
	}

	public void setPrev(LinkedListNode<T> prev) {
		this.prev = prev;
	}

	public LinkedListNode<T> getNext() {
		return next;
	}

	public void setNext(LinkedListNode<T> next) {
		this.next = next;
	}

}
