import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test {
	public static final String scmPath = "C:\\IBM\\SDP9\\scmtools\\eclipse\\scm.exe";

	public static void main(String[] args) {
	}
	
	public static void commandline() throws IOException, InterruptedException {
		Process p = Runtime.getRuntime().exec(scmPath);
		int exitVal = p.waitFor();
		System.out.println("Exited with code: " + exitVal);

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		// read the output from the command
		System.out.println("Here is the standard output of the command:\n");
		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}

		// read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
			System.out.println(s);
		}
	}
}
