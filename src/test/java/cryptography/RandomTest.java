package cryptography;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;

public class RandomTest {
	static Base32 b32 = new Base32();
	static Base64 b64 = new Base64();

	public static void main(String[] args) throws NoSuchAlgorithmException {
		SecureRandom random = SecureRandom.getInstanceStrong();
		BigInteger r = new BigInteger(1024, random);
		System.out.printf("%s\n", r.toString(2));
		System.out.printf("%s\n", r.toString(8));
		System.out.printf("%s\n", r.toString(16));
		System.out.printf("%s\n", r.toString(32));
		
		System.out.printf("%s\n", b32.encodeAsString(r.toByteArray()));
		System.out.printf("%s\n", b64.encodeAsString(r.toByteArray()));

		System.out.printf("%s\n", r);
	}
}
