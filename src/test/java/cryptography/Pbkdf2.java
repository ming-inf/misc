package cryptography;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

public class Pbkdf2 {
	public static void main(String[] args) throws UnsupportedEncodingException {
		Security.addProvider(new BouncyCastleProvider());

		byte[] key = encryptBC();
		String k = Base64.toBase64String(key);
		System.out.println(k);
	}

	public static byte[] encryptBC() throws UnsupportedEncodingException {
		PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA512Digest());
		gen.init("password".getBytes(StandardCharsets.UTF_8), createRandomArray(32), 4096);
		byte[] dk = ((KeyParameter)((ParametersWithIV)gen.generateDerivedParameters(512, 512)).getParameters()).getKey();
		return dk;
	}

	public static byte[] getEncryptedPasswordJava8(String password, byte[] salt, int iterations, int derivedKeyLength)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength * 8);

		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");

		return f.generateSecret(spec).getEncoded();
	}

	private static final SecureRandom secureRandom = new SecureRandom();

	private static byte[] createRandomArray(int size) {
		byte[] randomByteArray = new byte[size];
		secureRandom.nextBytes(randomByteArray);

		return randomByteArray;
	}
}
