package cryptography.srp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.agreement.srp.SRP6StandardGroups;
import org.bouncycastle.crypto.agreement.srp.SRP6VerifierGenerator;
import org.bouncycastle.crypto.digests.SkeinDigest;
import org.bouncycastle.crypto.macs.SkeinMac;
import org.bouncycastle.crypto.params.SRP6GroupParameters;
import org.bouncycastle.crypto.tls.DefaultTlsSRPGroupVerifier;
import org.bouncycastle.crypto.tls.SRPTlsServer;
import org.bouncycastle.crypto.tls.SimulatedTlsSRPIdentityManager;
import org.bouncycastle.crypto.tls.TlsSRPGroupVerifier;
import org.bouncycastle.crypto.tls.TlsSRPIdentityManager;
import org.bouncycastle.crypto.tls.TlsSRPLoginParameters;
import org.bouncycastle.crypto.tls.TlsServerProtocol;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

public class Server {
	public static final SRP6GroupParameters srp6GroupParameters = SRP6StandardGroups.rfc5054_8192;
	public static final TlsSRPGroupVerifier groupVerifier = new DefaultTlsSRPGroupVerifier();
	public static final int digestSizeBits = SkeinDigest.SKEIN_1024;
	public static final Digest digest = new SkeinDigest(digestSizeBits, digestSizeBits);
	public static final Mac mac = new SkeinMac(digestSizeBits, digestSizeBits);
	public static final SRP6VerifierGenerator gen = new SRP6VerifierGenerator();
	public static final byte[] seed = { 0 };
	public static final SecureRandom random = new SecureRandom(seed);

	public static String username = "username";
	public static String passphrase = "password";
	public static byte[] identity = username.getBytes();
	public static byte[] password = passphrase.getBytes();

	static {
		// add at runtime the Bouncy Castle Provider
		// the provider is available only for this application
		Security.addProvider(new BouncyCastleProvider());

		gen.init(srp6GroupParameters, digest);
	}

	public static final TlsSRPIdentityManager srpIdentityManager = new SimulatedTlsSRPIdentityManager(srp6GroupParameters,
			gen, mac) {
		byte[] s = Base64.decode(
				"erjchFbCXxMlUfFXx3oYiO+Rj6zM7tRGdRV8ziqRn5JhOFEdy5FBolxL9zmfkzsmPq6cylxRadrBy97HEpewyP3x7SELlfXtUW2sjCr8LjLZHaVLpdFPvQkTRdi5T2o96RRGN7yFy70q2HCzqy2yBEjzuLEAQmEVHHAd5Omqd9TjH2g6XGL4lD4DwJx4Qr0PcyRbbfbrUh2iDrTtNjHmRizU3OEfFD2WIgWf8q87ntO33siByqEzxumtwUHveBaDJ+ftOuGLHlhbtwbM8b2lD4XQ4xYNtWBDyU2uv4PX14u8utdrE6cUXCX0dnv1lOy+0I8JeR14iTX8NpTIKArnbn5sk2zLZklCJf1pPb80RAhZOYfLpbSDny2UrM4Rlhf1JQNgf2oxFEFpXMNE8Zim9F8q0dHwFRbIR1wExrP6HwyC/EwBmHLP+Dk1bLSSNfGacIHehDNku1RRgRREjx8AgSWA1fcBt2HmSgp6PN1w0niGnAtQZguKjMoPEKhW1ABrhcWFoXz76qtzINd8DMAbbliVgMtlNL0hIt+eqJ+whI+o1p7sgXtV+kiinbwcpdcWifh7/aRyGMBKo8+tubtefmdlxYLsX+H3IPlhtQFSUG/FFQUTX9SY/cSN5m1wtDRMLIDy3S2gP5y79SoE2FXp2wA9lZ2877mvLBY1wi77LEqTeIsOzS+Cs+7MjBP2drr6SwywKH6wmbMLlM2G3Xjnn/0HOoq1KOvmCQ48xje/uBDg8mcaWflFB1y2qFzeFHT2Mw25qTSS+UcCZqm0cjwcDwTNdCNLl9nfgG9CgC2Al415ltsZ8DpWHVDITkIVko3sOMnA/xU3Ah+9yRpTR9Duvq5NqP+W1HHRj/NfYPvegwroXHeqHO7wRh/qWJFLmQiGiQQiWD6baDR+BKdj4CB1eoW3hGz32RQtHyPWCIHQX0SJZC60Zvg8TwT+xPHTrakDThOAa+hOWehPJJ7d24smD292HqGGTTjY0gUF8kTSL8aXTXsNc6Oa/J+5VAi237QxqT2A4XC+Olzj68cpS/IXmvmYT/ohIW3Wvs8h5cSsiEP1jMEVwyY8YVB1S7MITZPeUNLdTCi0VVzGjcBMkbhZeAeUWiUmzF/LXgQ3a9XqESAxzX9QXGFJ+HuZFsGJbpEmoAWNWCtj+u/ueVBy4yHpH5oSu21b5pKeg3XT6Asj3V+cU7/ZR5sfvWLexo7lRjJGh+HByvjZFRWVApVOZ23QY6ov2XkZYUmqo0Hn7fGNd4IZylM4xSO5Dync5kMQ8Trlh/TRuFkQEq2ptrTLBBvkUAv0kCwMt6VSzrXkvAekkljF64KGpcqdCIRJ5qksj2mKEEhqcUM+prjKmwn+XTWfYg==");
		BigInteger v = gen.generateVerifier(s, identity, password);

		@Override
		public TlsSRPLoginParameters getLoginParameters(byte[] identity) {
			return new TlsSRPLoginParameters(srp6GroupParameters, v, s);
		}
	};

	public static void main(String[] args) throws IOException {
		SRPTlsServer server = new SRPTlsServer(srpIdentityManager);

		try (ServerSocket serverSocket = new ServerSocket(10443);
				Socket clientSocket = serverSocket.accept();
				PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));) {
			System.out.println("Started");
//			TlsServerProtocol protocol = new TlsServerProtocol(clientSocket.getInputStream(), clientSocket.getOutputStream(), random);
//			protocol.accept(server);
			
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				out.println(inputLine);
			}

//			protocol.close();
		}
	}
}
