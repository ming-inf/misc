package cryptography.srp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.agreement.srp.SRP6StandardGroups;
import org.bouncycastle.crypto.agreement.srp.SRP6VerifierGenerator;
import org.bouncycastle.crypto.digests.SkeinDigest;
import org.bouncycastle.crypto.macs.SkeinMac;
import org.bouncycastle.crypto.params.SRP6GroupParameters;
import org.bouncycastle.crypto.tls.DefaultTlsSRPGroupVerifier;
import org.bouncycastle.crypto.tls.SRPTlsClient;
import org.bouncycastle.crypto.tls.TlsClientProtocol;
import org.bouncycastle.crypto.tls.TlsSRPGroupVerifier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Client {
	public static final SRP6GroupParameters srp6GroupParameters = SRP6StandardGroups.rfc5054_8192;
	public static final TlsSRPGroupVerifier groupVerifier = new DefaultTlsSRPGroupVerifier();
	public static final int digestSizeBits = SkeinDigest.SKEIN_1024;
	public static final Digest digest = new SkeinDigest(digestSizeBits, digestSizeBits);
	public static final Mac mac = new SkeinMac(digestSizeBits, digestSizeBits);
	public static final SRP6VerifierGenerator gen = new SRP6VerifierGenerator();
	public static final byte[] seed = { 0 };
	public static final SecureRandom random = new SecureRandom(seed);

	public static String username = "username";
	public static String passphrase = "password";
	public static byte[] identity = username.getBytes();
	public static byte[] password = passphrase.getBytes();

	static {
		// add at runtime the Bouncy Castle Provider
		// the provider is available only for this application
		Security.addProvider(new BouncyCastleProvider());

		gen.init(srp6GroupParameters, digest);
	}

	public static void main(String[] args) throws UnknownHostException, IOException {
		SRPTlsClient client = new SRPTlsClient(identity, password);

		try (Socket socket = new Socket("localhost", 10443);
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {
//			TlsClientProtocol protocol = new TlsClientProtocol(socket.getInputStream(), socket.getOutputStream(), random);
//			protocol.connect(client);

			out.println("HEHE");

//			protocol.close();
		}
	}
}
