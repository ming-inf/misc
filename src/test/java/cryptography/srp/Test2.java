package cryptography.srp;

import java.io.Console;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.agreement.srp.SRP6Client;
import org.bouncycastle.crypto.agreement.srp.SRP6Server;
import org.bouncycastle.crypto.agreement.srp.SRP6StandardGroups;
import org.bouncycastle.crypto.agreement.srp.SRP6VerifierGenerator;
import org.bouncycastle.crypto.digests.SkeinDigest;
import org.bouncycastle.crypto.macs.SkeinMac;
import org.bouncycastle.crypto.params.SRP6GroupParameters;
import org.bouncycastle.crypto.tls.DefaultTlsSRPGroupVerifier;
import org.bouncycastle.crypto.tls.SimulatedTlsSRPIdentityManager;
import org.bouncycastle.crypto.tls.TlsSRPGroupVerifier;
import org.bouncycastle.crypto.tls.TlsSRPIdentityManager;
import org.bouncycastle.crypto.tls.TlsSRPLoginParameters;
import org.bouncycastle.crypto.tls.TlsServerProtocol;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.io.Streams;

public class Test2 {
	public static final SRP6GroupParameters srp6GroupParameters = SRP6StandardGroups.rfc5054_8192;
	public static final TlsSRPGroupVerifier groupVerifier = new DefaultTlsSRPGroupVerifier();
	public static final int digestSizeBits = SkeinDigest.SKEIN_1024;
	public static final Digest digest = new SkeinDigest(digestSizeBits, digestSizeBits);
	public static final Mac mac = new SkeinMac(digestSizeBits, digestSizeBits);
	public static final SRP6VerifierGenerator gen = new SRP6VerifierGenerator();
	public static final byte[] seed = { 0 };
	public static final SecureRandom random = new SecureRandom(seed);

	public static String username = "username";
	public static String passphrase = "password";
	public static byte[] identity = username.getBytes();
	public static byte[] password = passphrase.getBytes();

	static {
		// add at runtime the Bouncy Castle Provider
		// the provider is available only for this application
		Security.addProvider(new BouncyCastleProvider());

		gen.init(srp6GroupParameters, digest);
	}

	private static BigInteger fromBase64(String base64) {
		return new BigInteger(1, Base64.decode(base64));
	}

	public static final TlsSRPIdentityManager srpIdentityManager = new SimulatedTlsSRPIdentityManager(srp6GroupParameters,
			gen, mac) {
		byte[] s = Base64.decode(
				"erjchFbCXxMlUfFXx3oYiO+Rj6zM7tRGdRV8ziqRn5JhOFEdy5FBolxL9zmfkzsmPq6cylxRadrBy97HEpewyP3x7SELlfXtUW2sjCr8LjLZHaVLpdFPvQkTRdi5T2o96RRGN7yFy70q2HCzqy2yBEjzuLEAQmEVHHAd5Omqd9TjH2g6XGL4lD4DwJx4Qr0PcyRbbfbrUh2iDrTtNjHmRizU3OEfFD2WIgWf8q87ntO33siByqEzxumtwUHveBaDJ+ftOuGLHlhbtwbM8b2lD4XQ4xYNtWBDyU2uv4PX14u8utdrE6cUXCX0dnv1lOy+0I8JeR14iTX8NpTIKArnbn5sk2zLZklCJf1pPb80RAhZOYfLpbSDny2UrM4Rlhf1JQNgf2oxFEFpXMNE8Zim9F8q0dHwFRbIR1wExrP6HwyC/EwBmHLP+Dk1bLSSNfGacIHehDNku1RRgRREjx8AgSWA1fcBt2HmSgp6PN1w0niGnAtQZguKjMoPEKhW1ABrhcWFoXz76qtzINd8DMAbbliVgMtlNL0hIt+eqJ+whI+o1p7sgXtV+kiinbwcpdcWifh7/aRyGMBKo8+tubtefmdlxYLsX+H3IPlhtQFSUG/FFQUTX9SY/cSN5m1wtDRMLIDy3S2gP5y79SoE2FXp2wA9lZ2877mvLBY1wi77LEqTeIsOzS+Cs+7MjBP2drr6SwywKH6wmbMLlM2G3Xjnn/0HOoq1KOvmCQ48xje/uBDg8mcaWflFB1y2qFzeFHT2Mw25qTSS+UcCZqm0cjwcDwTNdCNLl9nfgG9CgC2Al415ltsZ8DpWHVDITkIVko3sOMnA/xU3Ah+9yRpTR9Duvq5NqP+W1HHRj/NfYPvegwroXHeqHO7wRh/qWJFLmQiGiQQiWD6baDR+BKdj4CB1eoW3hGz32RQtHyPWCIHQX0SJZC60Zvg8TwT+xPHTrakDThOAa+hOWehPJJ7d24smD292HqGGTTjY0gUF8kTSL8aXTXsNc6Oa/J+5VAi237QxqT2A4XC+Olzj68cpS/IXmvmYT/ohIW3Wvs8h5cSsiEP1jMEVwyY8YVB1S7MITZPeUNLdTCi0VVzGjcBMkbhZeAeUWiUmzF/LXgQ3a9XqESAxzX9QXGFJ+HuZFsGJbpEmoAWNWCtj+u/ueVBy4yHpH5oSu21b5pKeg3XT6Asj3V+cU7/ZR5sfvWLexo7lRjJGh+HByvjZFRWVApVOZ23QY6ov2XkZYUmqo0Hn7fGNd4IZylM4xSO5Dync5kMQ8Trlh/TRuFkQEq2ptrTLBBvkUAv0kCwMt6VSzrXkvAekkljF64KGpcqdCIRJ5qksj2mKEEhqcUM+prjKmwn+XTWfYg==");
		BigInteger v = gen.generateVerifier(s, identity, password);

		@Override
		public TlsSRPLoginParameters getLoginParameters(byte[] identity) {
			return new TlsSRPLoginParameters(srp6GroupParameters, v, s);
		}
	};

	public static void main(String[] args) throws Exception {
		Console c = System.console();
		c.readPassword("enter password %s:", "stuff");
	}

	static class ServerThread extends Thread {
		private final TlsServerProtocol serverProtocol;

		ServerThread(TlsServerProtocol serverProtocol) {
			this.serverProtocol = serverProtocol;
		}

		public void run() {
			try {
				Server2 server = new Server2();
				serverProtocol.accept(server);
				Streams.pipeAll(serverProtocol.getInputStream(), serverProtocol.getOutputStream());
				serverProtocol.close();
			} catch (Exception e) {
				// throw new RuntimeException(e);
			}
		}
	}

	public static void srp() throws Exception {
		byte[] s = Base64.decode(
				"erjchFbCXxMlUfFXx3oYiO+Rj6zM7tRGdRV8ziqRn5JhOFEdy5FBolxL9zmfkzsmPq6cylxRadrBy97HEpewyP3x7SELlfXtUW2sjCr8LjLZHaVLpdFPvQkTRdi5T2o96RRGN7yFy70q2HCzqy2yBEjzuLEAQmEVHHAd5Omqd9TjH2g6XGL4lD4DwJx4Qr0PcyRbbfbrUh2iDrTtNjHmRizU3OEfFD2WIgWf8q87ntO33siByqEzxumtwUHveBaDJ+ftOuGLHlhbtwbM8b2lD4XQ4xYNtWBDyU2uv4PX14u8utdrE6cUXCX0dnv1lOy+0I8JeR14iTX8NpTIKArnbn5sk2zLZklCJf1pPb80RAhZOYfLpbSDny2UrM4Rlhf1JQNgf2oxFEFpXMNE8Zim9F8q0dHwFRbIR1wExrP6HwyC/EwBmHLP+Dk1bLSSNfGacIHehDNku1RRgRREjx8AgSWA1fcBt2HmSgp6PN1w0niGnAtQZguKjMoPEKhW1ABrhcWFoXz76qtzINd8DMAbbliVgMtlNL0hIt+eqJ+whI+o1p7sgXtV+kiinbwcpdcWifh7/aRyGMBKo8+tubtefmdlxYLsX+H3IPlhtQFSUG/FFQUTX9SY/cSN5m1wtDRMLIDy3S2gP5y79SoE2FXp2wA9lZ2877mvLBY1wi77LEqTeIsOzS+Cs+7MjBP2drr6SwywKH6wmbMLlM2G3Xjnn/0HOoq1KOvmCQ48xje/uBDg8mcaWflFB1y2qFzeFHT2Mw25qTSS+UcCZqm0cjwcDwTNdCNLl9nfgG9CgC2Al415ltsZ8DpWHVDITkIVko3sOMnA/xU3Ah+9yRpTR9Duvq5NqP+W1HHRj/NfYPvegwroXHeqHO7wRh/qWJFLmQiGiQQiWD6baDR+BKdj4CB1eoW3hGz32RQtHyPWCIHQX0SJZC60Zvg8TwT+xPHTrakDThOAa+hOWehPJJ7d24smD292HqGGTTjY0gUF8kTSL8aXTXsNc6Oa/J+5VAi237QxqT2A4XC+Olzj68cpS/IXmvmYT/ohIW3Wvs8h5cSsiEP1jMEVwyY8YVB1S7MITZPeUNLdTCi0VVzGjcBMkbhZeAeUWiUmzF/LXgQ3a9XqESAxzX9QXGFJ+HuZFsGJbpEmoAWNWCtj+u/ueVBy4yHpH5oSu21b5pKeg3XT6Asj3V+cU7/ZR5sfvWLexo7lRjJGh+HByvjZFRWVApVOZ23QY6ov2XkZYUmqo0Hn7fGNd4IZylM4xSO5Dync5kMQ8Trlh/TRuFkQEq2ptrTLBBvkUAv0kCwMt6VSzrXkvAekkljF64KGpcqdCIRJ5qksj2mKEEhqcUM+prjKmwn+XTWfYg==");
		BigInteger v = gen.generateVerifier(s, identity, password);
		// System.out.printf("verifier: %s\n", Base64.toBase64String(v.toByteArray()));

		BigInteger a = fromBase64(
				"FaGI1VUMD7l1hcL6WR2vz1SMg1VolXnYRI8j1PzpgRUdBb6wfTl76gGBVKUL183BZgMd53r7p1Yp3BTAYH+ymm2vU3IQI0ZXs826FO8rKZhpKZ/Nhzxlqb6wiF/WEi3ZVefVzJ8O9rYD4UdGwwEguqHIS0u48Qq+OhJrbn18IVquP2mjO8GGZb8dt5YXJMssz9fMb7IJ2vA/8DDvISN9/Mo/6+Sz37w4tHa2N9ATMlpccV4GO4L7yIfCPIWMhbJyYXI04JjZlkoYvfA2VFGwY40ArR43HgDJli8AB7ehkL7wEp7a7F7fZxGrA/a+PLFJXbhyxae9TTsDzZb/G/EdfAdZywFKhP8rB4ndfK9GFInH1MGFsHEKCUIKqAOl/mcgCFNEIm4bgeM4wcbdYdQrdcqNNcZdth/i3cGwA/HEdc7lfaQM34qiq5KiZOirFN7Ezp038oCOHVraeBkMt5G/cQQCFpDs/0IuKSdB4gOA6+RZpJga53Pgjhnmbsv8ofJMao9+ZKajpXBtqYBwVVkHQ32Hx0bQjJFPmA431ih3XVEs3Vlb1go3yk5OCqU4IrV59zNdRIEkrTEMIp4xR3sUi+qYFqac9oiEWY3cm9iX1YC1XyRqPn6mAMcGZEA4kY660n3bktQ9bN1DuzkN+WfXHG+Mk/8/jO2hO7PmQ7o1WS7BFeCNdW0m1atKgGLY+cVmQ9JASrV6ic+aQ/u7O8PAtspwx9EQ7NWtSGGLjYYC+0Rafvmgh/OTgkBzhS/AWzrOsQg6G6DSm6LoAi42wS0tzH3TR3jKBlk3wuRzeqYQPe8HzXCfdKUox/R8rNwrl2cJDYcwmseiqCTT0rZQ/OcXvJRSwtMkvwTomnGUOoCfmu5MUvpnzxXbXHOV5aSPphaCbBsyls6vD+2Yt387YL8LtwTahK7iMDCGaAorgsuT7BvJF2j90qutxy86mgFOeSv3MShzLfbrstR3fAqbRMjbheogKEiYwEcTVNJXmnC8k3zhMpnSoSjGSw6nwUUlyA4GmP7QzuRX7GGuRH3mYQxlRBocipIzshpmwTSFkAp7fC7MLv42r0gvAJc8JHVUy70cITpllP4hUpugjjpK0AYjg2hVfXjTNDB15D0N7pnq6ODapdC2Yd8S7qkpeKg3z8KxLf1lSZAKu2x2MSMlV36KkghSosqqyWepkWx3Hp5VhrlGRmtJjQ0j5ao4OPxENHCcR/Xji0qfSVofGWpLAAWb0+WoFoX+3D+d6Tc9IhjbQ1jyj+5i7Lohk8QQnWmEwvTfcRvkOZqQliS6z2UtXqUElWF1YeK6nWef0o/AcPUldjW9F5AhIlyJ70V5SRYt9T8dfdkq1c9YEPvHiv6KQeCGjw==");
		BigInteger b = fromBase64(
				"BnwvUc6YI1nVwR2FbxU1eZgpcGYkNln9w69lMoWVgw8iGDmlhdU5rCjCPR7VwBUMVXRj/KqTJRXdOyTI1YQzS/XHbyb1mEQ+6ofaqJswm4o/Aa06pvvCyg6W0HBndCsyoKRgadJ/r3BzZJwmL5GT9Ux5fzgh739FM9VxTasr6SqgNvCrr2Lg+hdh3TAjNOOKF3r4Wyc9Kd2IoGhtHk99G+GNwTzGdoxf7S3eZKbyop3FG6yGu2jb6U9vVdFmYmEzj2PxPx3W4m5+3S1rJTFesfQJK94gJUgBwjvFK03AMfRZG2Paa+qG5fAExrQPyvLmr6KGo4i4SMx7rkao973a08evq3wBUkEQ52KSdmeQxEYBcC6tulARioDAmmM4K/akzgm1bMmOdcrYguzJ5KenG1A3YcnPS05HFBf1lqMnn6zTr6lcgB2dtJteFFpMWWZ6smHCcRjUze6ccW0cxz+W1u5NRDMzqH3fbthkOFmXhbjfCFGssrMrEcWtqenA8fEL8ov8ish4LL4EFq3NKdN8n4At3xKhsZcGlCAH47FjU7AmWAFnvVXmJ8dwP+JhsVvtdr3Qpniby5Eaefosb1K6besZbWTN+/BTPO+Pa8X/fN30YFj9JCDsS9mfD0FO8iQPFNd8JAZKiq5xUCcW8KcEvjhcvuFizLhbP6AyAT6v1lpywId7dal+NOjWeiqz2VaLRW6ZOp9ymGkKRrFBCGnJB6nh1TGY8GVKqL3SrkZOWNXsTZK1EfRvWRcMJfsIwUgAKsIk/DgXNuhUE35JIEG34YhA+umD+U67zip3i5LDdjBffYC5L+FZvvIUgkXIcnLqzprXKzv98n8N8tIhu6f79jghMld7iVgfIBD4yNMKEbwL8oFGcp/nwWfoixSOixXyv8BUOGzLJ16X23wWmhiBzUZbWniUcQ+Rye3E+cJdGzo40EUOPRF0LOOfj2RgboB2cIyZkAixjCdiAVmNtsVEpxM/f4xHZLAJViFQjLRF8fEsz123Esr8OPfewU0ZPFEZR/QYfl/JhqQQIAw/Ii5ovt768Xcgv2m24bB5eDa3DIpJ/D0TqX8x2Q8Ci7jNRfjGaIG3TcL0snly0Fsg07cUdZL6+EzcyDk+7qHTpaQiuUhOC6j44C/gMLDBfPUxNBKZ8caJFhdpewJW+hCOn3xUxcNDupCqVAy+6HYOMauO2Kz/hg7Yim1/ivF753EkRgoF2EOx2VaGZryiBSXRRE8L1elT921h/SMEFSFZoriaBjU4kjcnhdNe+Hk6KbRtXFf1xxQXHCbBvyNBAZhiGlR5ZllPapOw4e2l48htBacSvOlHSLXdooqAQaSJpfAlxks8+dpT0zC/hGwMZ56emtDd5Q==");

		final BigInteger aVal = a;
		SRP6Client client = new SRP6Client() {
			protected BigInteger selectPrivateValue() {
				return aVal;
			}
		};
		client.init(srp6GroupParameters, digest, random);

		final BigInteger bVal = b;
		SRP6Server server = new SRP6Server() {
			protected BigInteger selectPrivateValue() {
				return bVal;
			}
		};
		server.init(srp6GroupParameters, v, digest, random);

		BigInteger A = client.generateClientCredentials(s, identity, password);
		BigInteger B = server.generateServerCredentials();

		BigInteger clientS = client.calculateSecret(B);
		BigInteger serverS = server.calculateSecret(A);

		BigInteger clientM1 = client.calculateClientEvidenceMessage();
		boolean isM1Match = server.verifyClientEvidenceMessage(clientM1);

		BigInteger serverM2 = server.calculateServerEvidenceMessage();
		boolean isM2Match = client.verifyServerEvidenceMessage(serverM2);

		BigInteger clientK = client.calculateSessionKey();
		BigInteger serverK = server.calculateSessionKey();

		System.out.printf("isM1Match: %s, isM2Match: %s", isM1Match, isM2Match);
	}
}
