package cryptography;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Curve25519Test {
	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchProviderException,
			InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException {
		// following code from http://stackoverflow.com/a/33300707
		Security.addProvider(new BouncyCastleProvider());

		String name = "secp256r1";

		KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
		kpg.initialize(new ECGenParameterSpec(name));

		KeyPair keyPair = kpg.generateKeyPair();

		Cipher iesCipher = Cipher.getInstance("ECIESwithAES-CBC", BouncyCastleProvider.PROVIDER_NAME);
		iesCipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
		
		KeyFactory fact = KeyFactory.getInstance("EC", "BC");
		PublicKey publicKey = fact.generatePublic(new X509EncodedKeySpec(keyPair.getPublic().getEncoded()));
		PrivateKey privateKey = fact.generatePrivate(new PKCS8EncodedKeySpec(keyPair.getPrivate().getEncoded()));
		System.out.println(keyPair);
		
		// export keys
		// http://stackoverflow.com/a/20325322
		// http://stackoverflow.com/a/11969247
		// http://stackoverflow.com/a/11194304
		// http://serverfault.com/questions/9708/what-is-a-pem-file-and-how-does-it-differ-from-other-openssl-generated-key-file
	}

}
