package cryptography.ecc;

import java.util.Enumeration;

import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.jce.ECGOST3410NamedCurveTable;

public class ECCTest {
	public static void main(String[] args) {
		for (Enumeration e = ECNamedCurveTable.getNames(); e.hasMoreElements();) {
			System.out.printf("%s\n", e.nextElement());
		}
		System.out.println();
		for (Enumeration e = ECGOST3410NamedCurveTable.getNames(); e.hasMoreElements();) {
			System.out.printf("%s", e.nextElement());
		}
	}
}
