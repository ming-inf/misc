package cryptography;

import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

public class Scrypt {
	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());
		String expectedPassphrase = "7896d818e482e1bc47220bfed1e428c1c5055d0bbf33658144dc816e4ec8630d";
		String passphrase = "password";
		byte[] result = SCrypt.generate(passphrase.getBytes(), createRandomArray(32), 2^14, 8, 1, 32);
		System.out.println(Hex.toHexString(result));
		
		
	}

	private static final SecureRandom secureRandom = new SecureRandom();
	private static byte[] createRandomArray(int size) {
		byte[] randomByteArray = new byte[size];
		secureRandom.nextBytes(randomByteArray);

		return randomByteArray;
	}
}
