package cryptography;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.io.CipherOutputStream;
import org.bouncycastle.crypto.io.InvalidCipherTextIOException;
import org.bouncycastle.crypto.modes.AEADBlockCipher;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;

public class AesGcm {
	public static void main(String[] args) throws IOException, InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException {
		Security.addProvider(new BouncyCastleProvider());
		// Encrypt (not interesting in this example)
		byte[] randomKey = createRandomArray(16);
		byte[] randomIv = createRandomArray(16);
		byte[] originalPlaintext = "Confirm 100$ pay".getBytes("ASCII");
		byte[] originalCiphertext = encryptWithAesGcm(originalPlaintext, randomKey, randomIv);

		// Attack / alter ciphertext (an attacker would do this!)
		byte[] alteredCiphertext = Arrays.clone(originalCiphertext);
		alteredCiphertext[8] = (byte) (alteredCiphertext[8] ^ 0x08); // <<< Change 100$ to 900$

		// Decrypt with BouncyCastle implementation of CipherInputStream
		AEADBlockCipher cipher = new GCMBlockCipher(new AESEngine());
		cipher.init(false, new AEADParameters(new KeyParameter(randomKey), 128, randomIv));

		try {
			readFromStream(
					new org.bouncycastle.crypto.io.CipherInputStream(new ByteArrayInputStream(alteredCiphertext), cipher));
			// ^^^^^^^^^^^^^^^ INTERESTING PART ^^^^^^^^^^^^^^^^
			//
			// The BouncyCastle implementation of the CipherInputStream detects MAC verification errors and
			// throws a InvalidCipherTextIOException if an error occurs. Nice! A more or less minor issue
			// however is that it is incompatible with the standard JCE Cipher class from the javax.crypto
			// package. The new interface AEADBlockCipher must be used. The code below is not executed.

			// fail("Test D: org.bouncycastle.crypto.io.CipherInputStream: NOT OK, tampering not detected");
		} catch (InvalidCipherTextIOException e) {
			System.out.println("Test D: org.bouncycastle.crypto.io.CipherInputStream:        OK, tampering detected");
		}
	}

	private static byte[] readFromStream(InputStream inputStream) throws IOException {
		ByteArrayOutputStream decryptedPlaintextOutputStream = new ByteArrayOutputStream();

		int read = -1;
		byte[] buffer = new byte[16];

		while (-1 != (read = inputStream.read(buffer))) {
			decryptedPlaintextOutputStream.write(buffer, 0, read);
		}

		inputStream.close();
		decryptedPlaintextOutputStream.close();

		return decryptedPlaintextOutputStream.toByteArray();
	}

	private static byte[] encryptWithAesGcm(byte[] plaintext, byte[] randomKeyBytes, byte[] randomIvBytes)
			throws IOException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException {

		AEADBlockCipher cipher = new GCMBlockCipher(new AESEngine());
		cipher.init(true, new AEADParameters(new KeyParameter(randomKeyBytes), 128, randomIvBytes));

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		CipherOutputStream cipherOutputStream = new CipherOutputStream(byteArrayOutputStream, cipher);

		cipherOutputStream.write(plaintext);
		cipherOutputStream.close();

		return byteArrayOutputStream.toByteArray();
	}

	private static final SecureRandom secureRandom = new SecureRandom();
	private static byte[] createRandomArray(int size) {
		byte[] randomByteArray = new byte[size];
		secureRandom.nextBytes(randomByteArray);

		return randomByteArray;
	}
}
