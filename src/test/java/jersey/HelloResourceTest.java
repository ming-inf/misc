package jersey;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
	<dependency>
		<groupId>org.glassfish.jersey.test-framework.providers</groupId>
		<artifactId>jersey-test-framework-provider-grizzly2</artifactId>
		<version>2.22.1</version>
	</dependency>
	<dependency>
		<groupId>org.glassfish.jersey.media</groupId>
		<artifactId>jersey-media-json-jackson</artifactId>
	</dependency>
 *
 */
public class HelloResourceTest extends JerseyTest {

	static public class Entity1 {
		private String name = "e1";

		public Entity1() {
		}

		public Entity1(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	@Path("hello")
	public static class HelloResource {
		@GET
		public String getHello() {
			return "Hello World!";
		}

		@GET
		@Path("/getEntity")
		@Produces(MediaType.APPLICATION_JSON)
		public Entity1 getEntity1() {
			Entity1 e = new Entity1();
			return e;
		}

		@POST
		@Path("/echo")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Entity1 echo(Entity1 entity) {
			return entity;
		}
	}

	@Override
	protected Application configure() {
		return new ResourceConfig().registerClasses(HelloResource.class).register(JacksonFeature.class);
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(JacksonFeature.class);
	}

	@Test
	public void test() {
		final String hello = target("hello").request().get(String.class);
		Assert.assertEquals("Hello World!", hello);
	}

	@Test
	public void testJson() {
		final Entity1 entity = target("hello/getEntity").request(MediaType.APPLICATION_JSON).get(Entity1.class);
		Assert.assertEquals("e1", entity.getName());
	}

	@Test
	public void testEcho() {
		String name = "FEISFIJSEF";
		Entity1 testEntity = new Entity1(name);
		final Entity1 entity = target("hello/echo").request(MediaType.APPLICATION_JSON).post(Entity.entity(testEntity, MediaType.APPLICATION_JSON), Entity1.class);
		Assert.assertEquals(name, entity.getName());
	}
}