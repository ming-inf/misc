import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import algorithm.sort.comparison.ComparisonTestSuite;
import algorithm.sort.integer.IntegerTestSuite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	ComparisonTestSuite.class,
	IntegerTestSuite.class
})


public class AllTestSuite {

}
