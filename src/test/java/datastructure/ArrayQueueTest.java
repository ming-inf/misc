package datastructure;

import org.junit.Assert;
import org.junit.Test;

public class ArrayQueueTest {
	@Test
	public void enqueue() {
		ArrayQueue<Integer> q = new ArrayQueue<>();
		q.enqueue(1);
		Integer actual = q.dequeue().orNull();
		Assert.assertEquals((Integer)1, actual);
	}
}
