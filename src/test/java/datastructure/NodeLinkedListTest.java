package datastructure;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Optional;

import datastructure.api.LinkedListNode;

public class NodeLinkedListTest {
	@Test
	public void insertSearchThenDelete() {
		NodeLinkedList<Integer> l = new NodeLinkedList<>();
		l.insert(1);
		Optional<LinkedListNode<Integer>> search = l.search(1);
		Optional<Integer> actual = l.delete(search.get().get());
		Assert.assertEquals((Integer)1, actual.get());
	}
}
