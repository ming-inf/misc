package datastructure;

import org.junit.Assert;
import org.junit.Test;

public class ArrayStackTest {
	@Test
	public void pushPop() {
		ArrayStack<Integer> s = new ArrayStack<>();
		s.push(1);
		Assert.assertTrue(!s.stackEmpty());
		Integer actual = s.pop().orNull();
		Assert.assertEquals((Integer)1, actual);
	}
}
