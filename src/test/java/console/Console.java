package console;

import java.io.PrintWriter;
import java.io.Reader;

public abstract class Console {

	private static Console DEFAULT = (System.console() == null) ? new StandardConsole(System.in, System.out)
			: new SystemConsole(System.console());

	/**
	 * The default system text I/O device.
	 * 
	 * @return the default device
	 */
	public static Console defaultTextDevice() {
		return DEFAULT;
	}

	public abstract Console printf(String fmt, Object... params);

	public abstract String readLine();

	public abstract char[] readPassword();

	public abstract Reader reader();

	public abstract PrintWriter writer();
}
