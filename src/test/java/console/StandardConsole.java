package console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;

public class StandardConsole extends Console {
	private final BufferedReader reader;
	private final PrintWriter writer;

	public StandardConsole(InputStream in, OutputStream out) {
		this.reader = new BufferedReader(new InputStreamReader(in));
		this.writer = new PrintWriter(out, true);
	}

	public StandardConsole(BufferedReader reader, PrintWriter writer) {
		this.reader = reader;
		this.writer = writer;
	}

	@Override
	public StandardConsole printf(String fmt, Object... params) {
		writer.printf(fmt, params);
		return this;
	}

	@Override
	public String readLine() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public char[] readPassword() {
//	  char[] rcb = new char[1024];
//    int len = reader.read(rcb, 0, rcb.length);
//    if (len < 0)
//        return null;  //EOL
//    if (rcb[len-1] == '\r')
//        len--;        //remove CR at end;
//    else if (rcb[len-1] == '\n') {
//        len--;        //remove LF at end;
//        if (len > 0 && rcb[len-1] == '\r')
//            len--;    //remove the CR, if there is one
//    }
//    char[] b = new char[len];
//    if (len > 0) {
//        System.arraycopy(rcb, 0, b, 0, len);
//        if (zeroOut) {
//            Arrays.fill(rcb, 0, len, ' ');
//        }
//    }
//    return b;
		return readLine().toCharArray();
	}

	@Override
	public Reader reader() {
		return reader;
	}

	@Override
	public PrintWriter writer() {
		return writer;
	}
}
