package console;

import java.io.PrintWriter;
import java.io.Reader;

public class SystemConsole extends Console {
	private final java.io.Console console;

	public SystemConsole(java.io.Console console) {
		this.console = console;
	}

	@Override
	public Console printf(String fmt, Object... params) {
		console.format(fmt, params);
		return this;
	}

	@Override
	public Reader reader() {
		return console.reader();
	}

	@Override
	public String readLine() {
		return console.readLine();
	}

	@Override
	public char[] readPassword() {
		return console.readPassword();
	}

	@Override
	public PrintWriter writer() {
		return console.writer();
	}
}
