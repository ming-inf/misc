package algorithm.diff;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DigestByteProcessorTest {

	private static ExecutorService executor;

	@BeforeClass
	public static void beforeClass() {
		executor = Executors.newFixedThreadPool(3);
	}

	@Test
	public void digest() throws IOException, NoSuchAlgorithmException {
		DigestByteProcessor c = new DigestByteProcessor();
		BigInteger expected = new BigInteger(
				"4f285d0c0cc77286d8731798b7aae2639e28270d4166f40d769cbbdca5230714d848483d364e2f39fe6cb9083c15229b39a33615ebc6d57605f7c43f6906739d",
				16);
		byte[] actual = com.google.common.io.Files.readBytes(new File("abc"), c);
		Assert.assertEquals(expected, new BigInteger(1, actual));
	}

	@Test
	public void digest2() throws IOException, NoSuchAlgorithmException {
		DigestByteProcessor c = new DigestByteProcessor();
		BigInteger expected = new BigInteger(
				"4f285d0c0cc77286d8731798b7aae2639e28270d4166f40d769cbbdca5230714d848483d364e2f39fe6cb9083c15229b39a33615ebc6d57605f7c43f6906739d",
				16);
		byte[] actual = c.digest(new ByteArrayInputStream("abc\n".getBytes()));
		Assert.assertEquals(expected, new BigInteger(1, actual));
	}

	@Test
	public void compare() throws IOException, NoSuchAlgorithmException, InterruptedException, ExecutionException {
		boolean actual = DigestByteProcessor.compare(new FileInputStream(new File("abc")),
				new FileInputStream(new File("def")));
		Assert.assertTrue(actual);
	}

	DirectoryStream.Filter<Path> isDirectory = path -> Files.isDirectory(path);
	DirectoryStream.Filter<Path> isFile = path -> Files.isRegularFile(path);

	@Test
	public void digestDirectory() throws IOException, InterruptedException, ExecutionException {
		Map<Path, Pair<Future<byte[]>, InputStream>> allDigests = new TreeMap<>();

		Path directory = Paths.get(".");
		try (DirectoryStream<Path> files = Files.newDirectoryStream(directory, isFile)) {
			Stream<Path> stream = StreamSupport.stream(files.spliterator(), false);
			stream.forEach(file -> {
				try {
					InputStream is = Files.newInputStream(file);
					allDigests.put(file, Pair.of(executor.submit(DigestByteProcessor.digestCallable(is)), is));
				} catch (IOException e) {
				}
			});
		}

		for (Entry<Path, Pair<Future<byte[]>, InputStream>> entry : allDigests.entrySet()) {
			Pair<Future<byte[]>, InputStream> p = entry.getValue();
			// System.out.printf("%s %s\n", Base64.toBase64String(p.getLeft().get()), entry.getKey());
			p.getRight().close();
		}
	}

	@Test
	public void digestSubDirectory() throws IOException, InterruptedException, ExecutionException {
		Map<Path, Pair<Future<byte[]>, InputStream>> allDigests = new TreeMap<>();

		Path rootDirectory = Paths.get("");
		Stack<Path> stack = new Stack<>();
		stack.push(rootDirectory);

		while (!stack.isEmpty()) {
			Path directory = stack.pop();
			try (DirectoryStream<Path> ds = Files.newDirectoryStream(directory)) {
				Stream<Path> stream = StreamSupport.stream(ds.spliterator(), false);
				Map<Boolean, List<Path>> isDirectoryMap = stream
						.collect(Collectors.partitioningBy(path -> Files.isDirectory(path)));
				isDirectoryMap.get(true).stream().filter(dir -> dir.startsWith("lib")).filter(dir -> dir.startsWith(".git"))
						.filter(dir -> dir.startsWith("bin")).forEach(dir -> stack.push(dir));
				isDirectoryMap.get(false).stream().forEach(file -> {
					try {
						InputStream is = Files.newInputStream(file);
						allDigests.put(file, Pair.of(executor.submit(DigestByteProcessor.digestCallable(is)), is));
					} catch (IOException e) {
					}
				});
			}
		}

		for (Entry<Path, Pair<Future<byte[]>, InputStream>> entry : allDigests.entrySet()) {
			Pair<Future<byte[]>, InputStream> p = entry.getValue();
			System.out.printf("%s %s\n", Base64.toBase64String(p.getLeft().get()), entry.getKey());
			p.getRight().close();
		}
	}
}
