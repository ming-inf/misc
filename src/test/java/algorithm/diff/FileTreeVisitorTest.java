package algorithm.diff;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

public class FileTreeVisitorTest {
	@Test
	public void testTreeWalk() throws IOException {
		FileTreeVisitor visitor = new FileTreeVisitor();
		Files.walkFileTree(Paths.get(""), visitor);
	}
}
