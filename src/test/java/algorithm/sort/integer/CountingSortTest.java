package algorithm.sort.integer;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import algorithm.sort.integer.CountingSort;

public class CountingSortTest {

	@Test
	public void countingSort() {
		List<Integer> expected = asList(0, 0, 2, 2, 3, 3, 3, 5);
		List<Integer> actual = CountingSort.countingSort(Arrays.asList(2, 5, 3, 0, 2, 3, 0, 3), 5);
		assertEquals(expected, actual);
	}
	
	@Test
	public void countingSortByDigit() {
		List<Integer> expected = asList(720, 355, 436, 457, 657, 329, 839);
		List<Integer> actual = CountingSort.countingSortByDigit(Arrays.asList(329, 457, 657, 839, 436, 720, 355), 1);
		assertEquals(expected, actual);
	}

}
