package algorithm.sort.integer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	CountingSortTest.class,
	RadixSortTest.class
})

public class IntegerTestSuite {

}
