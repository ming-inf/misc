package algorithm.sort.integer;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import algorithm.sort.integer.RadixSort;

public class RadixSortTest {

	@Test
	public void radixSort() {
		List<Integer> expected = asList(329, 355, 436, 457, 657, 720, 839);
		List<Integer> actual = RadixSort.radixSort(asList(329, 457, 657, 839, 436, 720, 355), 3);
		assertEquals(expected, actual);
	}

}
