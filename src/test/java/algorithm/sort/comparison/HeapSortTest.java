package algorithm.sort.comparison;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import algorithm.sort.comparison.HeapSort;

public class HeapSortTest {

	@Test
	public void maxHeapify() {
		List<Integer> expected = asList(16, 14, 10, 8, 7, 9, 3, 2, 4, 1);
		List<Integer> actual = HeapSort.maxHeapify(Arrays.asList(16, 4, 10, 14, 7, 9, 3, 2, 8, 1), 2, 10);
		assertEquals(expected, actual);
	}

	@Test
	public void buildMaxHeap() {
		List<Integer> expected = asList(16, 14, 10, 8, 7, 9, 3, 2, 4, 1);
		List<Integer> actual = HeapSort.buildMaxHeap(Arrays.asList(4, 1, 3, 2, 16, 9, 10, 14, 8, 7));
		assertEquals(expected, actual);
	}

	@Test
	public void buildMinHeap() {
		List<Integer> expected = asList(1, 2, 3, 4, 7, 9, 10, 14, 8, 16);
		List<Integer> actual = HeapSort.buildMinHeap(Arrays.asList(4, 1, 3, 2, 16, 9, 10, 14, 8, 7));
		assertEquals(expected, actual);
	}

	@Test
	public void heapSort() {
		List<Integer> expected = asList(1, 2, 3, 4, 7, 8, 9, 10, 14, 16);
		List<Integer> actual = HeapSort.heapSort(Arrays.asList(4, 1, 3, 2, 16, 9, 10, 14, 8, 7));
		assertEquals(expected, actual);
	}

	@Test
	public void heapIncreaseKey() {
		List<Integer> expected = asList(16, 15, 10, 14, 7, 9, 3, 2, 8, 1);
		List<Integer> actual = HeapSort.heapIncreaseKey(Arrays.asList(16, 14, 10, 8, 7, 9, 3, 2, 4, 1), 9, 15);
		assertEquals(expected, actual);
	}

}
