package algorithm.sort.comparison;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	InsertionSortTest.class,
	MergeSortTest.class,
	HeapSortTest.class,
	QuickSortTest.class,
	MaxSubarrayTest.class
})

public class ComparisonTestSuite {

}
