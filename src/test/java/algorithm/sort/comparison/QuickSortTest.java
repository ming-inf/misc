package algorithm.sort.comparison;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import algorithm.sort.comparison.QuickSort;

public class QuickSortTest {

	@Test
	public void quickSort() {
		List<Integer> expected = asList(1, 2, 3, 4, 5, 6, 7, 8);
		List<Integer> actual = QuickSort.quickSort(Arrays.asList(2, 8, 7, 1, 3, 5, 6, 4), 1, 8);
		assertEquals(expected, actual);
	}

	@Test
	public void randomizedQuickSort() {
		List<Integer> expected = asList(1, 2, 3, 4, 5, 6, 7, 8);
		List<Integer> actual = QuickSort.randomizedQuickSort(Arrays.asList(2, 8, 7, 1, 3, 5, 6, 4), 1, 8);
		assertEquals(expected, actual);
	}

}
