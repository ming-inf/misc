package algorithm.sort.comparison;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import algorithm.sort.comparison.InsertionSort;
import algorithm.sort.comparison.InsertionSort.SortOrder;

public class InsertionSortTest {

	@Test
	public void insertionSortAscending() {
		List<Integer> expected = asList(1, 2, 3, 4, 5, 6);
		List<Integer> actual = InsertionSort.insertionSort(Arrays.asList(5, 2, 4, 6, 1, 3), SortOrder.ASCENDING);
		assertEquals(expected, actual);
	}

	@Test
	public void insertionSortDescending() {
		List<Integer> expected = asList(6, 5, 4, 3, 2, 1);
		List<Integer> actual = InsertionSort.insertionSort(Arrays.asList(5, 2, 4, 6, 1, 3), SortOrder.DESCENDING);
		assertEquals(expected, actual);
	}

	@Test
	public void insertionSortUnsorted() {
		List<Integer> expected = asList(5, 2, 4, 6, 1, 3);
		List<Integer> actual = InsertionSort.insertionSort(Arrays.asList(5, 2, 4, 6, 1, 3), SortOrder.UNSORTED);
		assertEquals(expected, actual);
	}

	@Test
	public void insertionRotateSortAscending() {
		List<Integer> expected = asList(1, 2, 3, 4, 5, 5, 6);
		List<Integer> actual = InsertionSort.insertionRotateSort(Arrays.asList(5, 5, 2, 4, 6, 1, 3), SortOrder.ASCENDING);
		assertEquals(expected, actual);
	}

	@Test
	public void insertionRotateSortDescending() {
		List<Integer> expected = asList(6, 5, 4, 3, 2, 1);
		List<Integer> actual = InsertionSort.insertionRotateSort(Arrays.asList(5, 2, 4, 6, 1, 3), SortOrder.DESCENDING);
		assertEquals(expected, actual);
	}

	@Test
	public void insertionRotateSortUnsorted() {
		List<Integer> expected = asList(5, 2, 4, 6, 1, 3);
		List<Integer> actual = InsertionSort.insertionRotateSort(Arrays.asList(5, 2, 4, 6, 1, 3), SortOrder.UNSORTED);
		assertEquals(expected, actual);
	}

}
