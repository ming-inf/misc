package algorithm.sort.comparison;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import algorithm.sort.comparison.MergeSort;
import algorithm.sort.comparison.InsertionSort.SortOrder;

public class MergeSortTest {

	@Test
	public void mergeSort() {
		List<Integer> expected = asList(1, 2, 2, 3, 4, 5, 6, 7);
		List<Integer> unordered = asList(5, 2, 4, 7, 1, 3, 2, 6);
		List<Integer> actual = MergeSort.mergeSort(unordered, 0, unordered.size(), SortOrder.ASCENDING);
		assertEquals(expected, actual);
	}

}
