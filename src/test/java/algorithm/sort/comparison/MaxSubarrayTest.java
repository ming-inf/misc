package algorithm.sort.comparison;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import algorithm.sort.comparison.MaxSubarray;

public class MaxSubarrayTest {

	@Test
	public void findMaximumSubarray() {
		List<Integer> expected = asList(7, 10, 43);
		List<Integer> array = Arrays.asList(13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7);
		List<Integer> actual = MaxSubarray.findMaximumSubarray(array, 0, array.size() - 1);
		assertEquals(expected, actual);
	}

	@Test
	public void maxSubArray() {
		int expected = 43;
		int actual = MaxSubarray
				.maxSubArray(new int[] { 13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7 });
		assertEquals(expected, actual);
	}

	@Test
	public void maxSubArrayDyn() {
		int expected = 43;
		int actual = MaxSubarray
				.maxSubArrayDyn(new int[] { 13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7 });
		assertEquals(expected, actual);
	}

}
