package javafx;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;

public class FxClipboardTest {
	public static void main(String[] args) {
		DataFormat fmt = new DataFormat("text/foo", "text/bar");
		Clipboard clipboard = Clipboard.getSystemClipboard();
		ClipboardContent content = new ClipboardContent();
		content.put(fmt, "Hello");
		clipboard.setContent(content);
	}
}
