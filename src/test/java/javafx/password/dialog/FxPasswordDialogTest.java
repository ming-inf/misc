package javafx.password.dialog;

import java.util.Optional;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class FxPasswordDialogTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setOnCloseRequest(e -> {
			Platform.exit();
			System.exit(0);
		});

		PasswordDialog pd = new PasswordDialog();
		Optional<String> result = pd.showAndWait();
		result.ifPresent(password -> System.out.println(password));
	}

}
