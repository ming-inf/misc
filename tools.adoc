Launchy – launch application with keystroke - http://www.launchy.net/
Search everything – search across your whole desktop -  https://www.voidtools.com/
conemu – tabbed windows terminal -  https://conemu.github.io/
tinytake – Screenshot tool - https://tinytake.com/
clipx – Retain multiple items in clipboard -  http://bluemars.org/clipx/
moboxterm – tabbed unix terminal - http://mobaxterm.mobatek.net/


