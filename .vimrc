" map semicolon to colon
:nmap ; :

" regain original functionality of ;
"nnoremap ; :
"nnoremap : ;
"vnoremap ; :
"vnoremap : ;

" map jk to esc
:imap jk <Esc>

" dark background
set bg=dark

" more natural split defaults
set splitbelow splitright

let g:netrw_browsex_viewer="cygstart"

noremap v <PageUp>
noremap <Space> <PageDown>
