# The following lines were added by compinstall
zstyle :compinstall filename '/home/c47451ac/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd notify
bindkey -v
# End of lines configured by zsh-newuser-install

export SCALA_HOME=~/scala-2.11.8
export SCM=/cygdrive/c/IBM/SDP9/scmtools/eclipse
export PATH=$SCM:$SCALA_HOME/bin:$PATH
if [ `uname -o` = 'Cygwin' ] ; then
 alias scala='scala -Djline.terminal=jline.UnixTerminal'
fi

# start tmux on login
#case $- in *i*)
#  if [ -z "$TMUX" ]; then exec `tmux new-session -A -s main`; fi;; # attach session or start new session
#esac

source ~/.alias

# change prompt in vi shell
if [ $VIM ]
then
  PROMPT="%/ VI "
else
  PROMPT="%/ "
fi
